# plexmediaserver2discord

Tiny project that notifies discord channel(s) when new content is available on plex.

# Notes 

Currently works on linux hosts (would work on other OSes, mind then to change multer dest)

# Mechanism

PlexMediaServer can use webhooks on specific events [plex events](https://support.plex.tv/articles/115002267687-webhooks/)  
Discord's integrations allow us to push information to channels [discord docs](https://support.discord.com/hc/en-us/articles/228383668-Intro-to-Webhooks)  
This project recieves Plex's webhooks, and forward new content webhook data to discord channel.

# Prerequisites

 * Plex premium pass (webhooks are locked otherwise)
 * Discord access to channel options

# Install

 * Clone the repo
 * then :
```bash
$ npm install
$ node plexmediaserver2discord.js
```

# Configuration

The app follows the [12 Factors](https://12factor.net/fr/) and as such, grabs its configuration from the environment.
To configure the app, the following environment variables are available

| Variable | Description | Type | Default value |
| -------- | ----------- | ---- | ------------- |
| ADDRESS | The server's listening address | String | `localhost` |
| PORT | The server's listening port | Number | `8042` |
| TMP_DIR | Where the server stores (temporarily) decoded payloads | String | `/tmp` |
| DISCORD_WEBHOOK | A comma-separated list of Discord webhooks URL | String | |
| DISCORD_BOT_NAME | The Discord bot name to use | String | `Plexus` |
| DISCORD_MESSAGE_PREFIX | The message prefix for outgoing Discord messages | String | ` Nouveau contenu: ` |

# Plex settings

 * Mandatory : add the the URI where to send notification (where plexmediaserver2discord is binded, when starting the projet gives it on the console)
 * Mandatory : activate push notifications
 * Optional : activate automatic library scanning for new content (otherwise do it manually)

 # Thanks

 DOLCIMASCOLO David for mentoring.
