const express     = require('express')
const multer      = require('multer');
const bodyParser  = require('body-parser');
const { Webhook } = require('discord-webhook-node');
const app         = express()
const hooks       = []

const config = {
  address: process.env.ADDRESS || 'localhost',
  port: process.env.PORT || 8042,
  tmpDir: process.env.TMP_DIR || '/tmp',
  botName: process.env.DISCORD_BOT_NAME || 'Plexus',
  message: process.env.DISCORD_MESSAGE_PREFIX || ' Nouveau contenu: '
}

if (process.env.DISCORD_WEBHOOK) {
  process.env.DISCORD_WEBHOOK.split(',').forEach(url => hooks.push(new Webhook(url)));
}

app.post('/', multer({ dest: config.tmpDir }).single('thumb'), function (req, res, next) {
  var payload = JSON.parse(req.body.payload);
  if(payload.event == 'library.new') {
    for(const hook of hooks) {
      hook.setUsername(config.botName);
      hook.send("@here " + config.message + " " + payload.Metadata.librarySectionTitle + " - " + payload.Metadata.title);
    }
  }
})

app.listen(config.port, config.address, () => {
  console.log(`plexmediaserver2discord app listening at : http://${config.address}:${config.port} (add this in Plex webhooks settings).`)
})
